package com.forever.sports.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author  Qi Song
 *
 * @since  15-03-17
 *
 */
public class Against {

    public Against() {
    }

    public Against(int roundNum) {
        this.roundNum = roundNum;
    }

    private  int roundNum;

    private List<AgainstPair> againstPairs = new ArrayList<AgainstPair>();

    public int getRoundNum() {
        return roundNum;
    }

    public void setRoundNum(int roundNum) {
        this.roundNum = roundNum;
    }

    public List<AgainstPair> getAgainstPairs() {
        return againstPairs;
    }

    public void setAgainstPairs(List<AgainstPair> againstPairs) {
        this.againstPairs = againstPairs;
    }
}
