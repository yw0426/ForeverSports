package com.forever.sports.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 *  时间 日期处理类
 *
 * @author Yuan Wei
 * @since  15-03-17
 *
 */
public class DateUtil {

	public final static String TIME_FORMAT_NO_SEPERATE = "yyyyMMdd";
	public final static String TIME_FORMAT_SEPERATE = "yyyy-MM-dd";
	public final static String DATE_SEC_FORMAT = "yyyy-MM-dd HH:mm:ss";

	private final static SimpleDateFormat DEFAULT_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     *  重新设置date的时、分、秒值
     * @param date
     * @param hour
     * @param min
     * @param sec
     * @return
     */
	public static Date changeTime(Date date, int hour, int min, int sec) {
		return setCalendarTime(date, hour, min, sec).getTime();
	}

	private static Calendar setCalendarTime(Date date, int hour, int min, int sec) {
		Calendar cal = Calendar.getInstance();
		if (null != date) {
			cal.setTime(date);
		}
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, min);
		cal.set(Calendar.SECOND, sec);
		cal.set(Calendar.MILLISECOND, 0);
		return cal;
	}

	/**
	 * note:interval should be revised to enum of Calendar, like Calendar.YEAR
	 * @param interval </br> 
	 * y -> Calendar.YEAR</br>  m -> Calendar.MONTH</br>  d -> Calendar.DATE</br>  h -> Calendar.HOUR</br>  minute -> Calendar.MINUTE
	 * @param number
	 * @param date
	 * @return
	 */
	public static Date dateAdd(String interval, int number, Date date) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		if (interval.equals("y")) {
			int currYear = gc.get(Calendar.YEAR);
			gc.set(Calendar.YEAR, currYear + number);
		} else if (interval.equals("m")) {
			int currMonth = gc.get(Calendar.MONTH);
			gc.set(Calendar.MONTH, currMonth + number);
		} else if (interval.equals("d")) {
			int currDay = gc.get(Calendar.DATE);
			gc.set(Calendar.DATE, currDay + number);
		} else if (interval.equals("h")) {
			int currDay = gc.get(Calendar.HOUR);
			gc.set(Calendar.HOUR, currDay + number);
		} else if (interval.equals("minute")) {
			int currDay = gc.get(Calendar.MINUTE);
			gc.set(Calendar.MINUTE, currDay + number);
		}
		return gc.getTime();
	}

	/**
	 * Give a date, like 2012-03-19, will parse it to 2012-03-19 00:00:00
	 * 
	 * @param date
	 * @return
	 */
	public static Date beginOfADay(Date date) {
		return setCalendarTime(date, 0, 0, 0).getTime();
	}

	/**
	 * Give a date, like 2012-03-19, will parse it to 2012-03-19 23:59:59
	 * 
	 * @param date
	 * @return
	 */
	public static Date endOfADay(Date date) {
		return setCalendarTime(date, 23, 59, 59).getTime();
	}

	public static Date beginOfMonth(Date date) {
		Calendar cal = setCalendarTime(date, 0, 0, 0);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}

	public static Date endOfMonth(Date date) {
		Calendar cal = setCalendarTime(date, 23, 59, 59);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		return cal.getTime();
	}

	/**
	 * 田忌上架判断上下架时间是都相同的函数
	 * 相差7天整或者7天的整数倍认为时间相同，否则认为不相同
	 * @param listTime
	 * @param delistTime
	 * @return
	 */
	public static Boolean isSimilarShelfTime(Date listTime, Date delistTime) {
		return isSimilarShelfTime(listTime, delistTime, 1);
	}
	
	public static Boolean isSimilarShelfTime(Date listTime, Date delistTime, Integer min) {
		if (listTime == null || delistTime == null || min == null) {
			return false;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(listTime);
		int listTimeDay = calendar.get(Calendar.DAY_OF_WEEK);
		int listTimeHours = calendar.get(Calendar.HOUR_OF_DAY);
		int listTimeMinutes = calendar.get(Calendar.MINUTE);
		calendar.setTime(delistTime);
		int delistTimeDay = calendar.get(Calendar.DAY_OF_WEEK);
		int delistTimeHours = calendar.get(Calendar.HOUR_OF_DAY);
		int delistTimeMinutes = calendar.get(Calendar.MINUTE);
		
		if (listTimeDay != delistTimeDay) {
			return false;
		}
		
		//误差到min分钟内,包含min
		if (Math.abs((delistTimeHours * 60 + delistTimeMinutes) - (listTimeHours * 60 + listTimeMinutes )) > min) {
			return false;
		}
		return true;
	}
	
	
	public static int getDay(Date date) {
		Calendar cal = Calendar.getInstance();
		if (null != date) {
			cal.setTime(date);
		}
		return cal.get(Calendar.DAY_OF_WEEK);
	}
	
	public static int getHour(Date date) {
		Calendar cal = Calendar.getInstance();
		if (null != date) {
			cal.setTime(date);
		}
		return cal.get(Calendar.HOUR_OF_DAY);
	}
	
	public static int getMin(Date date) {
		Calendar cal = Calendar.getInstance();
		if (null != date) {
			cal.setTime(date);
		}
		return cal.get(Calendar.MINUTE);
	}
}
