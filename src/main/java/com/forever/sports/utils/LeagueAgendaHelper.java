package com.forever.sports.utils;

import org.apache.commons.lang.time.DateUtils;

import java.text.SimpleDateFormat;
import java.util.*;

/** *
 * @author Yuan Wei
 *
 */
public class LeagueAgendaHelper {

    private static char[] valueMap = new char[]{'A', 'B', 'C', 'D', 'E', 'F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

    /**
     *
     * 根据输入指定的<params>球队数量</params>生成比赛赛程。
     *
     * @param num 球队数量
     * @return
     */
    public static List<Against> buildLeagueAgenda(int num, MatchType matchType){

        int[][] values= init(num);

        List<Against> againstList = new ArrayList<Against>();

        for (int i=1; i<num; i++){
            againstList.add(buildAgainst(values, num, i));
        }

        if (MatchType.Double.equals(matchType)){
            for (int i=num; i<2*num-1; i++){
                againstList.add(buildAgainst(values, num, i));
            }
        }

        return againstList;
    }

    private static Against buildAgainst(int[][] values, int num, int round){

        Against against = new Against(round);

        int r_tmp = round < num ? round : round+1-num;

        for(int x = 0; x < num; x++){
            for(int y = x+1; y< num; y++){
                if(values[x][y] == r_tmp ){
                    AgainstPair pair = new AgainstPair();
                    if (round<num){
                        pair.setTeam1(String.valueOf(valueMap[x]));
                        pair.setTeam2(String.valueOf(valueMap[y]));
                    } else {
                        pair.setTeam1(String.valueOf(valueMap[y]));
                        pair.setTeam2(String.valueOf(valueMap[x]));
                    }
                    against.getAgainstPairs().add(pair);
                }
            }
        }

        Collections.shuffle(against.getAgainstPairs());

        return against;
    }

    /**
     * 用指定的 num 初始化一个 二维数组， 二维数组 长, 宽 都等于 num
     *
     * @param num
     * @return
     */
	private static int[][] init(int num){
        int[][] values= new int[num][num];
        for(int x = 0; x < num; x++){
            for(int y = 0; y< num; y++){
            	values[x][y] = (x+y)%(num-1);
                if(values[x][y] == 0){
                	values[x][y] = num-1;
                }
            }
        }

        for(int x = 0; x < num; x++){
        	values[x][num-1] =values[x][x];
        	values[num-1][x] =values[x][x];
        }

        return values;
	}


    public static void main(String[] args) {

        List<Against> againsts = LeagueAgendaHelper.buildLeagueAgenda(16, MatchType.Single);
        for (Against against : againsts) {
            System.out.println("Round: " + against.getRoundNum() + ": " + against.getAgainstPairs());
        }

        List<Court> courts = new ArrayList<Court>();
        Court court = new Court();
        court.setVenues("hah1");
        court.setStartTime(new Date());
        courts.add(court);

        Court court0 = new Court();
        court0.setVenues("hah1");
        court0.setStartTime(new Date());
        courts.add(court0);

        Court court1 = new Court();
        court1.setVenues("hah2");
        court1.setStartTime(DateUtils.addHours(new Date(), 1));
        courts.add(court1);

        System.out.println("\n==========================================");
        System.out.println("Round: " + againsts.get(0).getRoundNum() + ": " +  againsts.get(0).getAgainstPairs());
        System.out.println("[Venues: " + court.getVenues() + ", Time: " + court.getStartTime() + "]Period: " + 7);

        List<LeagueAgenda> leagueAgenda = arrangement(againsts.get(0).getAgainstPairs(), courts, 7);

        for (LeagueAgenda la : leagueAgenda) {
            System.out.println("[Venues: " + la.getCourt().getVenues() + ", Time: " + new SimpleDateFormat("yy-MM-dd HH:mm").format(la.getCourt().getStartTime()) + "]: " + la.getAgainstPair());
        }
    }


    public static  List<LeagueAgenda> arrangement(List<AgainstPair> againstPairs, List<Court> courts, int period){
        List<LeagueAgenda> leagueAgendas = new ArrayList<LeagueAgenda>();
        int index = 0;
        int len = courts.size();
        for (AgainstPair againstPair : againstPairs) {
            LeagueAgenda leagueAgenda = new LeagueAgenda();
            leagueAgenda.setAgainstPair(againstPair);
            int i = index % len;
            Court court = courts.get(i);
            court.setStartTime(DateUtils.addDays(court.getStartTime(), period));
            court.setFirstHalfStartTime(DateUtils.addDays(court.getFirstHalfStartTime(), period));
            court.setFirstHalfEndTime(DateUtils.addDays(court.getFirstHalfEndTime(), period));
            court.setSecondHalfStartTime(DateUtils.addDays(court.getSecondHalfStartTime(), period));
            court.setSecondHalfEndTime(DateUtils.addDays(court.getSecondHalfEndTime(), period));
            court.setStartTime(DateUtils.addDays(court.getStartTime(), period));
            leagueAgenda.setCourt(new Court(court));
            leagueAgendas.add(leagueAgenda);
            index++;
        }
        return leagueAgendas;

    }

}
