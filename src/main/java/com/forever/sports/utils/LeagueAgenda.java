package com.forever.sports.utils;

import java.util.Date;

/**
 * @author  Qi Song
 *
 * @since  15-03-18
 *
 */
public class LeagueAgenda {

    private AgainstPair againstPair;

    private Court court;

    public AgainstPair getAgainstPair() {
        return againstPair;
    }

    public void setAgainstPair(AgainstPair againstPair) {
        this.againstPair = againstPair;
    }

    public Court getCourt() {
        return court;
    }

    public void setCourt(Court court) {
        this.court = court;
    }
}
