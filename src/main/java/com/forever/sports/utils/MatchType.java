package com.forever.sports.utils;

/**
 * @author  Qi Song
 *
 * @since  15-03-17
 *
 */
public enum MatchType {

    Single("单循环赛"), Double("双循环赛") ;

    private String value;

    MatchType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}


