package com.forever.sports.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author  Qi Song
 *
 * @since  15-03-18
 *
 */
public class Court {

    private String venues; //场馆

    private Date startTime; // 比赛开始时间

    private Date endTime; // 比赛结束时间

    private Date firstHalfStartTime; // 上半场比赛开始时间

    private Date firstHalfEndTime; // 上半场比赛结束时间

    private Date secondHalfStartTime; // 下半场比赛开始时间

    private Date secondHalfEndTime; // 下半场比赛结束时间

    public String getVenues() {
        return venues;
    }

    public void setVenues(String venues) {
        this.venues = venues;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getFirstHalfStartTime() {
        return firstHalfStartTime;
    }

    public void setFirstHalfStartTime(Date firstHalfStartTime) {
        this.firstHalfStartTime = firstHalfStartTime;
    }

    public Date getFirstHalfEndTime() {
        return firstHalfEndTime;
    }

    public void setFirstHalfEndTime(Date firstHalfEndTime) {
        this.firstHalfEndTime = firstHalfEndTime;
    }

    public Date getSecondHalfStartTime() {
        return secondHalfStartTime;
    }

    public void setSecondHalfStartTime(Date secondHalfStartTime) {
        this.secondHalfStartTime = secondHalfStartTime;
    }

    public Date getSecondHalfEndTime() {
        return secondHalfEndTime;
    }

    public void setSecondHalfEndTime(Date secondHalfEndTime) {
        this.secondHalfEndTime = secondHalfEndTime;
    }

    public Court() {
    }

    public Court(Court old) {
        this.venues = old.getVenues();
        this.startTime = old.getStartTime();
        this.firstHalfStartTime = old.getFirstHalfStartTime();
        this.firstHalfEndTime = old.getFirstHalfEndTime();
        this.secondHalfStartTime = old.getSecondHalfStartTime();
        this.secondHalfEndTime = old.getSecondHalfEndTime();
    }

    @Override
    public String toString() {
        return "Court{" +
                "venues='" + venues + '\'' +
                ", startTime=" + new SimpleDateFormat("yy-MM-dd HH:mm").format(startTime) +
                ", endTime=" + new SimpleDateFormat("yy-MM-dd HH:mm").format(endTime) +
                ", startTime=" + new SimpleDateFormat("yy-MM-dd HH:mm").format(firstHalfStartTime) +
                ", endTime=" + new SimpleDateFormat("yy-MM-dd HH:mm").format(firstHalfEndTime) +
                ", startTime=" + new SimpleDateFormat("yy-MM-dd HH:mm").format(secondHalfStartTime) +
                ", endTime=" + new SimpleDateFormat("yy-MM-dd HH:mm").format(secondHalfEndTime) +
                '}';
    }
}
