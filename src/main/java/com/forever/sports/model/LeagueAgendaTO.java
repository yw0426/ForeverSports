package com.forever.sports.model;

import java.util.List;

/**
 * @author yw
 *
 */
public class LeagueAgendaTO {
	
	private int num;
	
	private List<String> agendaList;
	
	private String team1;
	
	private String team2;
	
	private int leagueId;
	
	private int leagueRound;

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public List<String> getAgendaList() {
		return agendaList;
	}

	public void setAgendaList(List<String> agendaList) {
		this.agendaList = agendaList;
	}

	public String getTeam1() {
		return team1;
	}

	public void setTeam1(String team1) {
		this.team1 = team1;
	}

	public String getTeam2() {
		return team2;
	}

	public void setTeam2(String team2) {
		this.team2 = team2;
	}

	public int getLeagueId() {
		return leagueId;
	}

	public void setLeagueId(int leagueId) {
		this.leagueId = leagueId;
	}

	public int getLeagueRound() {
		return leagueRound;
	}

	public void setLeagueRound(int leagueRound) {
		this.leagueRound = leagueRound;
	}
	
	
	
}
