package com.forever.sports.model.football;

import java.util.Date;

public class LeagueAgendaTO {
	
	private int cldId;
	
	private int matchId;
	
	private int leagueId;
	
	private String leagueName;
	
	private int round;
	
	private Date reacDate;
	
	private int courtId;
	
	private String firstHalfStartTime;

	private String firstHalfEndTime;
	
	private String secondHalfStartTime;
	
	private String sencondHalfEndTime;
	
	private int teamAId;
	
	private String teamAName;
	
	private int teamAScore;
	
	private int teamAPoint;
	
	private int teamBId;
	
	private String teamBName;
	
	private int teamBScore;
	
	private int teamBPoint;

	public int getCldId() {
		return cldId;
	}

	public void setCldId(int cldId) {
		this.cldId = cldId;
	}

	public int getLeagueId() {
		return leagueId;
	}

	public void setLeagueId(int leagueId) {
		this.leagueId = leagueId;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public Date getReacDate() {
		return reacDate;
	}

	public void setReacDate(Date reacDate) {
		this.reacDate = reacDate;
	}

	public int getCourtId() {
		return courtId;
	}

	public void setCourtId(int courtId) {
		this.courtId = courtId;
	}

	public String getFirstHalfStartTime() {
		return firstHalfStartTime;
	}

	public void setFirstHalfStartTime(String firstHalfStartTime) {
		this.firstHalfStartTime = firstHalfStartTime;
	}

	public String getFirstHalfEndTime() {
		return firstHalfEndTime;
	}

	public void setFirstHalfEndTime(String firstHalfEndTime) {
		this.firstHalfEndTime = firstHalfEndTime;
	}

	public String getSecondHalfStartTime() {
		return secondHalfStartTime;
	}

	public void setSecondHalfStartTime(String secondHalfStartTime) {
		this.secondHalfStartTime = secondHalfStartTime;
	}

	public String getSencondHalfEndTime() {
		return sencondHalfEndTime;
	}

	public void setSencondHalfEndTime(String sencondHalfEndTime) {
		this.sencondHalfEndTime = sencondHalfEndTime;
	}

	public int getTeamAId() {
		return teamAId;
	}

	public void setTeamAId(int teamAId) {
		this.teamAId = teamAId;
	}

	public int getTeamAScore() {
		return teamAScore;
	}

	public void setTeamAScore(int teamAScore) {
		this.teamAScore = teamAScore;
	}

	public int getTeamAPoint() {
		return teamAPoint;
	}

	public void setTeamAPoint(int teamAPoint) {
		this.teamAPoint = teamAPoint;
	}

	public int getTeamBId() {
		return teamBId;
	}

	public void setTeamBId(int teamBId) {
		this.teamBId = teamBId;
	}

	public int getTeamBScore() {
		return teamBScore;
	}

	public void setTeamBScore(int teamBScore) {
		this.teamBScore = teamBScore;
	}

	public int getTeamBPoint() {
		return teamBPoint;
	}

	public void setTeamBPoint(int teamBPoint) {
		this.teamBPoint = teamBPoint;
	}
	
	public String getLeagueName() {
		return leagueName;
	}

	public void setLeagueName(String leagueName) {
		this.leagueName = leagueName;
	}

	public String getTeamAName() {
		return teamAName;
	}

	public void setTeamAName(String teamAName) {
		this.teamAName = teamAName;
	}

	public String getTeamBName() {
		return teamBName;
	}

	public void setTeamBName(String teamBName) {
		this.teamBName = teamBName;
	}

	public int getMatchId() {
		return matchId;
	}

	public void setMatchId(int matchId) {
		this.matchId = matchId;
	}
}
