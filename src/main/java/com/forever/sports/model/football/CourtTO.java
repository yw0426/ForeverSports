package com.forever.sports.model.football;

public class CourtTO {
	private int courtId;
	
	private String courtName;
	
	private String courtAddress;
	
	private boolean nightInd;
	
	private String courtContactPhone;
	
	private String courtContactName;

	public int getCourtId() {
		return courtId;
	}

	public void setCourtId(int courtId) {
		this.courtId = courtId;
	}

	public String getCourtName() {
		return courtName;
	}

	public void setCourtName(String courtName) {
		this.courtName = courtName;
	}

	public String getCourtAddress() {
		return courtAddress;
	}

	public void setCourtAddress(String courtAddress) {
		this.courtAddress = courtAddress;
	}

	public boolean isNightInd() {
		return nightInd;
	}

	public void setNightInd(boolean nightInd) {
		this.nightInd = nightInd;
	}

	public String getCourtContactPhone() {
		return courtContactPhone;
	}

	public void setCourtContactPhone(String courtContactPhone) {
		this.courtContactPhone = courtContactPhone;
	}

	public String getCourtContactName() {
		return courtContactName;
	}

	public void setCourtContactName(String courtContactName) {
		this.courtContactName = courtContactName;
	}
	
	
}
