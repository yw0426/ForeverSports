package com.forever.sports.model;

/**
 * @author yw
 *
 */
public class ScorerTO {

	private int num;
	
	private String teamName;

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	
	
}
