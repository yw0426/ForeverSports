package com.forever.sports.web.action;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.forever.sports.utils.Court;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import com.forever.sports.model.football.LeagueAgendaTO;
import com.forever.sports.model.football.LeagueTO;
import com.forever.sports.utils.Against;
import com.forever.sports.utils.MatchType;
import com.forever.sports.web.service.footballService.ILeagueService;
import com.forever.sports.web.service.footballService.TeamService;
import com.forever.sports.web.service.footballService.impl.LeagueAgendaServiceImpl;
//yw
public class LeagueAgendaController extends BaseController{

	private LeagueAgendaServiceImpl leagueAgendaService;

    private TeamService teamService;
    
    private ILeagueService leagueService;

    private DateFormat dateFormat = new SimpleDateFormat("HH:mm");

//    public ModelAndView teamnameinput(HttpServletRequest request, HttpServletResponse response){
//
//        String leagueName = request.getParameter("leagueName");
//        int teamNumber = Integer.parseInt(request.getParameter("teamNumber"));
//        String matchType = request.getParameter("matchType");
//        LeagueTO leagueTO = new LeagueTO();
//        leagueTO.setLeagueName(leagueName);
//        leagueTO.setTeamNumber(teamNumber);
//        leagueTO.setMatchType(matchType);
//        leagueAgendaService.saveLeagueInfo(leagueTO);
//
//        ModelAndView mv = new ModelAndView("football.teamNameInput");
//        mv.addObject("leagueName", leagueName);
//        mv.addObject("teamNumber", teamNumber);
//
//        return mv;
//    }

    public ModelAndView viewCreateLeague(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mv = new ModelAndView("viewCreateLeague");
		return mv;
	}
    
	public ModelAndView createAgenda(HttpServletRequest request, HttpServletResponse response){
		LeagueTO leagueTO = new LeagueTO();
		int teamNumber = Integer.parseInt(request.getParameter("teamNumber"));
		String leagueName = request.getParameter("leagueName");
		String matchType = request.getParameter("matchType");
		
		leagueTO.setTeamNumber(teamNumber);
		leagueTO.setMatchType(matchType);
		leagueTO.setLeagueName(leagueName);
		
		LeagueTO leagueTODB = leagueService.queryLeagueIdByLeagueName(leagueName);
		
		if(leagueTODB!=null){
			leagueService.updateLeagueInfoByLeagueName(leagueTO);
		}else{
			leagueService.saveLeagueInfo(leagueTO);
		}
    	ModelAndView mv = new ModelAndView("football.leagueAgenda");

//        String[] teamNames = request.getParameterValues("teamName");
//        if (teamNames!=null && teamNames.length !=0 ){
//            teamService.saveFootballTeams(leagueName, teamNames);
//        }

        List<Against>  againstList = leagueAgendaService.buildCalendar(leagueName, teamNumber,MatchType.valueOf(matchType));

    	mv.addObject("list", againstList);
    	//LeagueAgendaHelper agenda = new LeagueAgendaHelper(teamNumber);
    	
    	mv.addObject("teamNumber", teamNumber);
    	mv.addObject("leagueName", leagueName);
    	mv.addObject("matchType", matchType);
    	
    	//mv.addObject("list", agenda.getLeagueAgendas());
    	return mv;
	}

	public ModelAndView defineLeagueDetail(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mv = new ModelAndView("defineLeagueDetail");
		int leagueId = Integer.parseInt(request.getParameter("leagueId"));
		LeagueTO leagueTO = new LeagueTO();
		leagueTO.setLeagueId(leagueId);
        Set<String> teams = leagueAgendaService.getAllTeamNames(leagueId);
        mv.addObject("teams", teams);

		mv.addObject("leagueId", leagueId);
		return mv;
	}
	
	public ModelAndView activeLeagueHomePage(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mv = new ModelAndView("activeLeagueHomePage");
		request.getParameter("list");
		
		List<LeagueTO> leagueList = leagueService.getActiveLeagueList();
		mv.addObject("leagueList",leagueList);
		return mv;
	}
	
	public ModelAndView getLeagueAgendaByRound(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mv = new ModelAndView("leagueAgendaByRound");
		List<LeagueAgendaTO> laList = leagueAgendaService.getLeagueAgendaByRound(1);
		mv.addObject("list", laList);
		return mv;
	}

	private void updateLeagueDetail(LeagueTO leagueTO){
		leagueService.updateLeagueInfoByLeagueID(leagueTO);
	}
	
	public ModelAndView defineCourtAndTime(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mv = new ModelAndView("defineCourtAndTime");
		
		int leagueId = Integer.parseInt(request.getParameter("leagueId"));
		int matchNumber = Integer.parseInt(request.getParameter("matchNumber"));
		int fieldNumber = Integer.parseInt(request.getParameter("fieldNumber"));
		int intervalDays = Integer.parseInt(request.getParameter("intervalDays"));
		
		LeagueTO leagueTO = new LeagueTO();
		leagueTO.setLeagueId(leagueId);
		leagueTO.setMatchNumber(matchNumber);
		leagueTO.setFieldNumber(fieldNumber);
		leagueTO.setIntervalDays(intervalDays);
		
		String leagueStartDateString = (request.getParameter("leagueStartDate"));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date leagueStartDate;
		try {
			leagueStartDate = sdf.parse(leagueStartDateString);
			leagueTO.setLeagueStartDate(leagueStartDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.updateLeagueDetail(leagueTO);

        Enumeration enumeration = request.getParameterNames();
        Map<String, String> map = new HashMap<String, String>();
        while (enumeration.hasMoreElements()){
            String name = (String)enumeration.nextElement();
            if (StringUtils.isNotBlank(name) && name.startsWith("team_") && StringUtils.isNotBlank(request.getParameter(name))){
                String value = request.getParameter(name);
                map.put(name.replace("team_", ""), value);
            }
        }
        leagueAgendaService.saveFootballTeams(leagueId, map);

		mv.addObject("fieldNumber",fieldNumber);
		mv.addObject("matchNumber",matchNumber);
		mv.addObject("leagueId",leagueId);
		return mv;
	}
	
	public ModelAndView finalAgenda(HttpServletRequest request, HttpServletResponse response){

        ModelAndView mv = new ModelAndView("finalAgenda");

		int leagueId = Integer.parseInt(request.getParameter("leagueId"));

        List<Court> courtList = buildCourts(request);
        for (Court court : courtList) {
            System.out.println(court);
        }
        leagueAgendaService.arrangement(leagueId,  courtList, 7);

		return mv;
	}

    private List<Court> buildCourts(HttpServletRequest request){

        int fieldNumber = Integer.parseInt(request.getParameter("fieldNumber"));
        int matchNumber = Integer.parseInt(request.getParameter("matchNumber"));

        System.out.println("fieldNumber: " + fieldNumber + "; matchNumber: " + matchNumber);

        List<Court> courtList = new ArrayList<Court>();
        for (int i=1; i<=fieldNumber; i++){
            String fieldName = request.getParameter("fieldName_" + i);
            for (int j=1; j<=matchNumber; j++){
                Court court = new Court();
                court.setVenues(fieldName);
                String firstHalfStartTime = request.getParameter("firstHalfStartTime_" + i + "_" + j);
                String firstHalfEndTime = request.getParameter("firstHalfEndTime_" + i + "_" + j);
                String secondHalfStartTime = request.getParameter("secondHalfStartTime_" + i + "_" + j);
                String sencondHalfEndTime = request.getParameter("sencondHalfEndTime_" + i + "_" + j);

                System.out.println( i + "_" + j + ": " + firstHalfStartTime);
                System.out.println( i + "_" + j + ": " + firstHalfEndTime);
                System.out.println( i + "_" + j + ": " + secondHalfStartTime);
                System.out.println( i + "_" + j + ": " + sencondHalfEndTime);

                try {
                    court.setStartTime(dateFormat.parse(firstHalfStartTime));
                    court.setFirstHalfStartTime(dateFormat.parse(firstHalfStartTime));
                    court.setFirstHalfEndTime(dateFormat.parse(firstHalfEndTime));
                    court.setSecondHalfStartTime(dateFormat.parse(secondHalfStartTime));
                    court.setSecondHalfEndTime(dateFormat.parse(sencondHalfEndTime));
                    court.setEndTime(dateFormat.parse(sencondHalfEndTime));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                courtList.add(court);
            }
        }

        return courtList;

    }
	
	public ILeagueService getLeagueService() {
		return leagueService;
	}

	public void setLeagueService(ILeagueService leagueService) {
		this.leagueService = leagueService;
	}
	
	public TeamService getTeamService() {
        return teamService;
    }

    public void setTeamService(TeamService teamService) {
        this.teamService = teamService;
    }

    public LeagueAgendaServiceImpl getLeagueAgendaService() {
		return leagueAgendaService;
	}

	public void setLeagueAgendaService(LeagueAgendaServiceImpl leagueAgendaService) {
		this.leagueAgendaService = leagueAgendaService;
	}
	
}
