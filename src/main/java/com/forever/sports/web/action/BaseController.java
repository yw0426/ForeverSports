package com.forever.sports.web.action;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author yw
 * test for 20150118 by yuanwei
 * test for 20150118 by yuanwei222
 * test for 20150118 by yuanwei333
 * @Since 14-10-22.
 */
public class BaseController extends MultiActionController {

    protected final Log logger = LogFactory.getLog(getClass());

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

        logger.info("URI: " + request.getRequestURI());

        return super.handleRequest(request, response);
    }
}
