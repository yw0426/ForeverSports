package com.forever.sports.web.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.forever.sports.utils.LeagueAgendaHelper;
import org.springframework.web.servlet.ModelAndView;

import com.forever.sports.model.ScorerTO;
import com.forever.sports.web.service.footballService.TeamService;

/**
 * @Author yw
 * sc 第二次提交 第二次test
 * sc 第san次提交 第san次test
 * @Since 14-10-22.
 */

public class FootballLeagueScorerController extends BaseController {
	
	private TeamService teamService;

    public ModelAndView scorer(HttpServletRequest request, HttpServletResponse response) {
    	String a = "gogogo";
        ModelAndView mv = new ModelAndView("league.scorer");
        mv.addObject("a", a);
        List<ScorerTO> st = new ArrayList<ScorerTO>();
        ScorerTO s = new ScorerTO();
        ScorerTO c = new ScorerTO();
        ScorerTO b = new ScorerTO();
        
        s.setNum(1);
        c.setNum(2);
        b.setNum(3);
        s.setTeamName("aa");
        c.setTeamName("bb");
        b.setTeamName("cc");
        
        st.add(s);
        st.add(b);
        st.add(c);
        
        mv.addObject("st", st);
        
        return mv;
    }

    public ModelAndView agenda(HttpServletRequest request, HttpServletResponse response) {
    	String m = request.getParameter("n");
    	if (m==null || m==""){
    		m="2";
    	}
    	
    	ModelAndView mv = new ModelAndView("football.leagueAgenda");
    	mv.addObject("m", m);
    	
    	//LeagueAgendaHelper agenda = new LeagueAgendaHelper(Integer.parseInt(m));
    	
    	//mv.addObject("list", agenda.getLeagueAgendas());
    	
    	return mv;
    }
    
    public ModelAndView team(HttpServletRequest request, HttpServletResponse response) {
    	ModelAndView mv = new ModelAndView("football.team");
    
    	//List<FootballTeamTO>  teamList = teamService.viewTeam();
    	//mv.addObject("teamList", teamList);
    	return mv;
    }

	public TeamService getTeamService() {
		return teamService;
	}

	public void setTeamService(TeamService teamService) {
		this.teamService = teamService;
	}
     
}
