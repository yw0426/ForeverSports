package com.forever.sports.web.action;

import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author yw
 * @Since 14-10-22.
 */
public class HomeController extends BaseController {

    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mv = new ModelAndView("football.index");
        return mv;
    }

}
