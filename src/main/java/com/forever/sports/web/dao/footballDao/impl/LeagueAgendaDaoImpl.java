package com.forever.sports.web.dao.footballDao.impl;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.forever.sports.model.football.LeagueAgendaTO;
import com.forever.sports.model.football.LeagueTO;
import com.forever.sports.web.dao.footballDao.ILeagueAgendaDao;

public class LeagueAgendaDaoImpl extends SqlMapClientDaoSupport implements ILeagueAgendaDao{

	public List<LeagueAgendaTO> getLeagueAgendaByRound(int round) {
		return (List<LeagueAgendaTO>)super.getSqlMapClientTemplate().queryForList("football.queryLeagueAgendaByRound", round);
	}

    @Override
    public void saveLeagueAgendaTO(LeagueAgendaTO leagueAgendaTO) {
        super.getSqlMapClientTemplate().insert("football.insertleagueAgendaInfo", leagueAgendaTO);
    }

	@Override
	public List<LeagueAgendaTO> queryLeagueAgendaByLeagueId(int leagueId) {
		return super.getSqlMapClientTemplate().queryForList("football.queryLeagueAgendaByLeagueId", leagueId);
	}

	@Override
	public void deleteLeagueAgendaByLeagueId(int leagueId) {
		super.getSqlMapClientTemplate().delete("football.deleteLeagueAgendaByLeagueId", leagueId);
	}

    @Override
    public void updateTeamName(LeagueAgendaTO leagueAgendaTO) {
        super.getSqlMapClientTemplate().update("football.updateleagueAgendaTeamName", leagueAgendaTO);
    }

    @Override
    public void updateDate(LeagueAgendaTO leagueAgendaTO) {
        super.getSqlMapClientTemplate().update("football.updateleagueAgendaDate", leagueAgendaTO);
    }
}
