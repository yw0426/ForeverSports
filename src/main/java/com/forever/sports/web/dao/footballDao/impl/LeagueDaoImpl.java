package com.forever.sports.web.dao.footballDao.impl;

import com.forever.sports.model.football.LeagueTO;
import com.forever.sports.web.dao.footballDao.ILeagueDao;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import java.util.List;

public class LeagueDaoImpl extends SqlMapClientDaoSupport implements ILeagueDao {

	public void saveLeagueInfo(LeagueTO leagueTO) {
		super.getSqlMapClientTemplate().insert("football.insertLeagueInfo", leagueTO);
		
	}

    @Override
    public LeagueTO queryLeagueIdByLeagueName(String leagueName) {
        return (LeagueTO)super.getSqlMapClientTemplate().queryForObject("football.queryLeagueIdByLeagueName", leagueName);
    }

	@Override
	public List<LeagueTO> getActiveLeagueList() {
		return (List<LeagueTO>) super.getSqlMapClientTemplate().queryForList("football.getLeagueList");
	}

	@Override
	public LeagueTO getLeagueInfoByLeagueId(LeagueTO leagueTO) {
		return (LeagueTO)super.getSqlMapClientTemplate().queryForObject("football.getLeagueInfoByLeagueId", leagueTO);
	}

	@Override
	public void updateLeagueInfoByLeagueName(LeagueTO leagueTO) {
		super.getSqlMapClientTemplate().update("football.updateLeagueInfoByLeagueName", leagueTO);
	}

	@Override
	public void updateLeagueInfoByLeagueID(LeagueTO leagueTO) {
		super.getSqlMapClientTemplate().update("football.updateLeagueInfoByLeagueID", leagueTO);
	}
	
}
