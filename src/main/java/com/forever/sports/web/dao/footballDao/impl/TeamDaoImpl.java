package com.forever.sports.web.dao.footballDao.impl;

import java.util.List;

import com.forever.sports.model.football.TeamTO;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.forever.sports.model.FootballTeamTO;
import com.forever.sports.web.dao.footballDao.TeamDao;

public class TeamDaoImpl extends SqlMapClientDaoSupport implements TeamDao {

	public List<TeamTO> viewTeam() {
		return super.getSqlMapClientTemplate().queryForList("football.queryAllTeam");
	}

    @Override
    public void saveTeam(TeamTO teamTO) {
        super.getSqlMapClientTemplate().insert("football.insertLeagueTeam", teamTO);
    }
}
