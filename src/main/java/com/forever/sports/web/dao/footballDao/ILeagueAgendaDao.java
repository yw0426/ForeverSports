package com.forever.sports.web.dao.footballDao;

import java.util.List;

import com.forever.sports.model.football.LeagueAgendaTO;

public interface ILeagueAgendaDao {
	public List<LeagueAgendaTO> getLeagueAgendaByRound(int round);

    public void saveLeagueAgendaTO(LeagueAgendaTO leagueAgendaTO);
    
    public List<LeagueAgendaTO> queryLeagueAgendaByLeagueId(int leagueId);
    
    public void deleteLeagueAgendaByLeagueId(int leagueId);

    void updateTeamName(LeagueAgendaTO leagueAgendaTO);

    void updateDate(LeagueAgendaTO to);
}
