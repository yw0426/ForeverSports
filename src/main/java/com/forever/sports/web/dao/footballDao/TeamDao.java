package com.forever.sports.web.dao.footballDao;

import java.util.List;

import com.forever.sports.model.FootballTeamTO;
import com.forever.sports.model.football.LeagueAgendaTO;
import com.forever.sports.model.football.TeamTO;

public interface TeamDao {
	public List<TeamTO> viewTeam();


    public void saveTeam(TeamTO footballTeamTO);

}
