package com.forever.sports.web.dao.footballDao;

import com.forever.sports.model.football.LeagueAgendaTO;
import com.forever.sports.model.football.LeagueTO;

import java.util.List;

public interface ILeagueDao {
	
	public void saveLeagueInfo(LeagueTO leagueTO);

    public LeagueTO queryLeagueIdByLeagueName(String leagueName);

    public List<LeagueTO> getActiveLeagueList();
    
    public LeagueTO getLeagueInfoByLeagueId(LeagueTO leagueTO);
    
    public void updateLeagueInfoByLeagueName(LeagueTO leagueTO);

    public void updateLeagueInfoByLeagueID(LeagueTO leagueTO);
    
}
