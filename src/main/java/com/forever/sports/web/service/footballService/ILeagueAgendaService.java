package com.forever.sports.web.service.footballService;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.forever.sports.model.football.LeagueAgendaTO;
import com.forever.sports.model.football.LeagueTO;
import com.forever.sports.utils.Against;
import com.forever.sports.utils.Court;
import com.forever.sports.utils.MatchType;

public interface ILeagueAgendaService {
	public List<LeagueAgendaTO> getLeagueAgendaByRound(int round);
	
    public LeagueTO getLeagueAgenda(String  leagueName);

    /**
     *
     *  生成 赛程 表
     *
     * @param leagueName  联赛名字
     * @param num  球队个数
     */
    public List<Against> buildCalendar(String leagueName, int num,MatchType matchType);

    /**
     *
     *  生成 赛程 表
     *
     * @param leagueId  联赛ID
     * @param leagueName 联赛名字
     * @param num  球队个数
     */
    public List<Against> buildCalendar(int leagueId, String leagueName,  int num,MatchType matchType);
    
    public boolean queryLeagueAgendaByLeagueId(int leagueId);    	
    
    public void deleteLeagueAgendaByLeagueId(int leagueId);

    public long saveFootballTeams(int leagueId, Map<String, String> teamNameMap);

    public Set<String> getAllTeamNames(int leagueId);

    void arrangement(int leagueId, List<Court> courtList, int period);
}
