package com.forever.sports.web.service.footballService.impl;

import java.util.List;

import com.forever.sports.model.football.TeamTO;
import com.forever.sports.web.dao.footballDao.ILeagueDao;
import com.forever.sports.web.dao.footballDao.TeamDao;
import com.forever.sports.web.service.footballService.TeamService;
//yw
public class TeamServiceImpl implements TeamService{

	private TeamDao teamDao;

    private ILeagueDao leagueDao;

    @Override
    public long saveFootballTeams(int leagueId, String[] teamNames) {

        for (String teamName : teamNames) {
            TeamTO teamTO = new TeamTO();
            teamTO.setTeamName(teamName);
            teamTO.setLeagueId(leagueId);
            teamDao.saveTeam(teamTO);
        }

        return 0;
    }

    public List<TeamTO> viewTeam() {
		return teamDao.viewTeam();
	}

    public TeamDao getTeamDao() {
		return teamDao;
	}

	public void setTeamDao(TeamDao teamDao) {
		this.teamDao = teamDao;
	}

    public ILeagueDao getLeagueDao() {
        return leagueDao;
    }

    public void setLeagueDao(ILeagueDao leagueDao) {
        this.leagueDao = leagueDao;
    }


}
