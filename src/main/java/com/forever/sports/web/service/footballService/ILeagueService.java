package com.forever.sports.web.service.footballService;

import java.util.List;

import com.forever.sports.model.football.LeagueTO;


public interface ILeagueService {
	public List<LeagueTO> getActiveLeagueList();
	
	public void saveLeagueInfo(LeagueTO leagueTO);
	
	public LeagueTO getLeagueInfoByLeagueId(LeagueTO leagueTO);
	
	public LeagueTO queryLeagueIdByLeagueName(String leagueName);
	
	public void updateLeagueInfoByLeagueName(LeagueTO leagueTO);
	
	public void updateLeagueInfoByLeagueID(LeagueTO leagueTO);
	
}
