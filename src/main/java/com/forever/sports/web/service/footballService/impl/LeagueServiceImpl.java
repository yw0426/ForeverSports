package com.forever.sports.web.service.footballService.impl;

import java.util.List;

import com.forever.sports.model.football.LeagueTO;
import com.forever.sports.web.dao.footballDao.ILeagueDao;
import com.forever.sports.web.service.footballService.ILeagueService;

public class LeagueServiceImpl implements ILeagueService{

	private ILeagueDao leagueDao;

	public ILeagueDao getLeagueDao() {
		return leagueDao;
	}

	public void setLeagueDao(ILeagueDao leagueDao) {
		this.leagueDao = leagueDao;
	}

	@Override
	public List<LeagueTO> getActiveLeagueList() {
		return leagueDao.getActiveLeagueList();
	}
	
	@Override
	public LeagueTO getLeagueInfoByLeagueId(LeagueTO leagueTO) {
		return leagueDao.getLeagueInfoByLeagueId(leagueTO);
	}

	@Override
	public void saveLeagueInfo(LeagueTO leagueTO) {
		leagueDao.saveLeagueInfo(leagueTO);
	}

	@Override
	public LeagueTO queryLeagueIdByLeagueName(String leagueName) {
		return leagueDao.queryLeagueIdByLeagueName(leagueName);
	}

	@Override
	public void updateLeagueInfoByLeagueName(LeagueTO leagueTO) {
		leagueDao.updateLeagueInfoByLeagueName(leagueTO);
	}

	@Override
	public void updateLeagueInfoByLeagueID(LeagueTO leagueTO) {
		leagueDao.updateLeagueInfoByLeagueID(leagueTO);
	}
}
