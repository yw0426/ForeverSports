package com.forever.sports.web.service.footballService.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import com.forever.sports.model.football.LeagueAgendaTO;
import com.forever.sports.model.football.LeagueTO;
import com.forever.sports.utils.*;
import com.forever.sports.web.dao.footballDao.ILeagueAgendaDao;
import com.forever.sports.web.dao.footballDao.ILeagueDao;
import com.forever.sports.web.service.footballService.ILeagueAgendaService;
import org.springframework.util.CollectionUtils;

public class LeagueAgendaServiceImpl implements ILeagueAgendaService {

    private static DateFormat dateFormat = new SimpleDateFormat("yy-MM-dd HH:mm");

	private ILeagueAgendaDao leagueAgendaDao;

	private ILeagueDao leagueDao;

    @Override
    public void arrangement(int leagueId, List<Court> courtList, int period) {

        List<LeagueAgendaTO> leagueAgendaTOs = leagueAgendaDao.queryLeagueAgendaByLeagueId(leagueId);

        List<AgainstPair> againstPairs =  new ArrayList<AgainstPair>();
        Map<AgainstPair, LeagueAgendaTO> map = new HashMap<AgainstPair, LeagueAgendaTO>();
        for (LeagueAgendaTO leagueAgendaTO : leagueAgendaTOs) {
            AgainstPair againstPair = new AgainstPair();
            againstPair.setTeam1(leagueAgendaTO.getTeamAName());
            againstPair.setTeam2(leagueAgendaTO.getTeamBName());
            againstPairs.add(againstPair);
            map.put(againstPair, leagueAgendaTO);
        }

        List<LeagueAgenda> leagueAgenda = LeagueAgendaHelper.arrangement(againstPairs, courtList, period);

        for (LeagueAgenda agenda : leagueAgenda) {
            AgainstPair againstPair = agenda.getAgainstPair();
            LeagueAgendaTO to = map.get(againstPair);
            Court court = agenda.getCourt();
            to.setFirstHalfStartTime(dateFormat.format(court.getFirstHalfStartTime()));
            to.setFirstHalfEndTime(dateFormat.format(court.getFirstHalfEndTime()));
            to.setSecondHalfStartTime(dateFormat.format(court.getSecondHalfStartTime()));
            to.setSencondHalfEndTime(dateFormat.format(court.getSecondHalfEndTime()));
            leagueAgendaDao.updateDate(to);
        }
    }

    @Override
    public Set<String> getAllTeamNames(int leagueId) {
        List<LeagueAgendaTO> leagueAgendaTOs = leagueAgendaDao.queryLeagueAgendaByLeagueId(leagueId);
        if (CollectionUtils.isEmpty(leagueAgendaTOs)){
            return Collections.EMPTY_SET;
        }

        Set<String> nameSet = new HashSet<String>();
        for (LeagueAgendaTO leagueAgendaTO : leagueAgendaTOs) {
            nameSet.add(leagueAgendaTO.getTeamAName());
            nameSet.add(leagueAgendaTO.getTeamBName());
        }

        return nameSet;
    }

    @Override
    public long saveFootballTeams(int leagueId, Map<String, String> teamNameMap) {

        List<LeagueAgendaTO> leagueAgendaTOs = leagueAgendaDao.queryLeagueAgendaByLeagueId(leagueId);

        for (LeagueAgendaTO leagueAgendaTO : leagueAgendaTOs) {
            String nameA = leagueAgendaTO.getTeamAName();
            String nameB = leagueAgendaTO.getTeamBName();
            if (teamNameMap.containsKey(nameA)){
                leagueAgendaTO.setTeamAName(teamNameMap.get(nameA));
            }
            if (teamNameMap.containsKey(nameB)){
                leagueAgendaTO.setTeamBName(teamNameMap.get(nameB));
            }
            leagueAgendaDao.updateTeamName(leagueAgendaTO);
        }

        return 0;
    }

    @Override
	public LeagueTO getLeagueAgenda(String leagueName) {
		return leagueDao.queryLeagueIdByLeagueName(leagueName);
	}

	@Override
	public List<Against> buildCalendar(String leagueName, int num, MatchType matchType) {
		LeagueTO leagueTO = leagueDao.queryLeagueIdByLeagueName(leagueName);
		return buildCalendar(leagueTO.getLeagueId(), leagueTO.getLeagueName(),num, matchType);
	}

	@Override
	public List<Against> buildCalendar(int leagueId, String leagueName, int num, MatchType matchType) {
		System.out.println("[BuildCalendar][LeagueId: " + leagueId + "][LeagueName : " + leagueName + "][Num: " + num + "]");
		List<Against> againstList = LeagueAgendaHelper.buildLeagueAgenda(num, matchType);
		for (Against against : againstList) {
			System.out.println("Round: " + against.getRoundNum() + ": " + against.getAgainstPairs());
		}

		// 查询leagueId 是否存在，若存在，则删除原来的赛程，再新增新的排序后的赛程
		boolean isExist = this.queryLeagueAgendaByLeagueId(leagueId);
		if (isExist) {// 如果有该联赛id的赛程，就先删掉
			this.deleteLeagueAgendaByLeagueId(leagueId);
		}
		int matchId = 1;
		for (Against against : againstList) {
			int round = against.getRoundNum();
			for (AgainstPair pair : against.getAgainstPairs()) {
				LeagueAgendaTO leagueAgendaTO = new LeagueAgendaTO();
				leagueAgendaTO.setLeagueId(leagueId);
				leagueAgendaTO.setLeagueName(leagueName);
				leagueAgendaTO.setRound(round);
				leagueAgendaTO.setTeamAName(pair.getTeam1());
				leagueAgendaTO.setTeamBName(pair.getTeam2());
				leagueAgendaTO.setMatchId(matchId);
				leagueAgendaDao.saveLeagueAgendaTO(leagueAgendaTO);
				matchId++;
			}
		}
		return againstList;
	}

	@Override
	public boolean queryLeagueAgendaByLeagueId(int leagueId) {
		List<LeagueAgendaTO> leagueAgendaTOList = leagueAgendaDao.queryLeagueAgendaByLeagueId(leagueId);
		if (leagueAgendaTOList.size() == 0 || leagueAgendaTOList == null) {
			return false;//不存在
		} else
			return true;//存在
	}

	@Override
	public void deleteLeagueAgendaByLeagueId(int leagueId) {
		leagueAgendaDao.deleteLeagueAgendaByLeagueId(leagueId);
	}

	public List<LeagueAgendaTO> getLeagueAgendaByRound(int round) {
		return leagueAgendaDao.getLeagueAgendaByRound(round);
	}

	public void setLeagueAgendaDao(ILeagueAgendaDao leagueAgendaDao) {
		this.leagueAgendaDao = leagueAgendaDao;
	}

	public ILeagueDao getLeagueDao() {
		return leagueDao;
	}

	public void setLeagueDao(ILeagueDao leagueDao) {
		this.leagueDao = leagueDao;
	}
}
