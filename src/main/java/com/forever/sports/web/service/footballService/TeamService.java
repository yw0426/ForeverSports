package com.forever.sports.web.service.footballService;

import java.util.*;

import com.forever.sports.model.FootballTeamTO;
import com.forever.sports.model.football.TeamTO;
//yw
public interface TeamService {
	public List<TeamTO> viewTeam();

    public long  saveFootballTeams(int leagueId, String[] teamNames);
}
