<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="/common/include.jsp" %>
<tiles:importAttribute scope="request"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--[if lt IE 7 ]><html lang="en" class="ie6 ielt7 ielt8 ielt9"><![endif]-->
<!--[if IE 7 ]><html lang="en" class="ie7 ielt8 ielt9"><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie8 ielt9"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Forever Sports</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="${contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="${contextPath}/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="${contextPath}/css/site.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>

<body>


<div class="container">
    <div class="navbar">
        <tiles:insertAttribute name="header"/>
    </div>
    <div class="row">
        <div class="span3">
            <tiles:insertAttribute name="menu_l"/>
        </div>
        <div class="span9">
            <tiles:insertAttribute name="content"/>
        </div>
    </div>
</div>
<script src="${contextPath}/js/jquery-1.8.3.min.js"></script>
<script src="${contextPath}/js/bootstrap.min.js"></script>
<script src="${contextPath}/js/bootstrap-datetimepicker.min.js"></script>
<script src="${contextPath}/js/locales/bootstrap-datetimepicker.fr.js"></script>
<script src="${contextPath}/js/site.js"></script>

</body>

</html>