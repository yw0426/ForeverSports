<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include.jsp"%>

        <h2>
            Recent Activity
        </h2>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>
                    Project
                </th>
                <th>
                    Client
                </th>
                <th>
                    Type
                </th>
                <th>
                    Date
                </th>
                <th>
                    View
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    Nike.com Redesign
                </td>
                <td>
                    Monsters Inc
                </td>
                <td>
                    New Task
                </td>
                <td>
                    4 days ago
                </td>
                <td>
                    <a href="#" class="view-link">View</a>
                </td>
            </tr>
            <tr>
                <td>
                    Nike.com Redesign
                </td>
                <td>
                    Monsters Inc
                </td>
                <td>
                    New Message
                </td>
                <td>
                    5 days ago
                </td>
                <td>
                    <a href="#" class="view-link">View</a>
                </td>
            </tr>
            <tr>
                <td>
                    Nike.com Redesign
                </td>
                <td>
                    Monsters Inc
                </td>
                <td>
                    New Project
                </td>
                <td>
                    5 days ago
                </td>
                <td>
                    <a href="#" class="view-link">View</a>
                </td>
            </tr>
            <tr>
                <td>
                    Twitter Server Consulting
                </td>
                <td>
                    Bad Robot
                </td>
                <td>
                    New Task
                </td>
                <td>
                    6 days ago
                </td>
                <td>
                    <a href="#" class="view-link">View</a>
                </td>
            </tr>
            <tr>
                <td>
                    Childrens Book Illustration
                </td>
                <td>
                    Evil Genius
                </td>
                <td>
                    New Message
                </td>
                <td>
                    9 days ago
                </td>
                <td>
                    <a href="#" class="view-link">View</a>
                </td>
            </tr>
            <tr>
                <td>
                    Twitter Server Consulting
                </td>
                <td>
                    Bad Robot
                </td>
                <td>
                    New Task
                </td>
                <td>
                    16 days ago
                </td>
                <td>
                    <a href="#" class="view-link">View</a>
                </td>
            </tr>
            <tr>
                <td>
                    Twitter Server Consulting
                </td>
                <td>
                    Bad Robot
                </td>
                <td>
                    New Project
                </td>
                <td>
                    16 days ago
                </td>
                <td>
                    <a href="#" class="view-link">View</a>
                </td>
            </tr>
            <tr>
                <td>
                    Twitter Server Proposal
                </td>
                <td>
                    Bad Robot
                </td>
                <td>
                    Completed Project
                </td>
                <td>
                    20 days ago
                </td>
                <td>
                    <a href="#" class="view-link">View</a>
                </td>
            </tr>
            </tbody>
        </table>

