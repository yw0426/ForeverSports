<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include.jsp"%>

<script type="text/javascript">
	function defineCourtAndTime(){
		var url="/foreversports/football/league/defineCourtAndTime.html";
		$("#createLeageuForm").attr("action", url);
		$("#createLeageuForm").sumit();
	}
</script>

<form method="post" id="createLeageuForm" action="defineCourtAndTime.html">
<h1>Forever Sports</h1>
<div class="hero-unit">
	<table width="700px">
		<tr>
			<td>每日比赛场次数：</td>
			<td><input id="matchNumber" name="matchNumber" value="">
			<input type="hidden" name = "leagueId" value="${leagueId}">
			</td>
		</tr>
		<tr>
			<td>同时开赛场次数：</td>
			<td><input id="fieldNumber" name="fieldNumber" value="">
			</td>
		</tr>
		<tr>
			<td>联赛开始日期 ：</td>
			<td><div class="controls input-append date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input1" data-link-format="yyyy-mm-dd">
                <input name="leagueStartDate" size="8" type="text" value="" readonly>
                <span class="add-on"><i class="icon-remove"></i></span>
                <span class="add-on"><i class="icon-th"></i></span>
            </div>
			</td>
		</tr>
		<tr>
			<td>比赛间隔天数：</td>
			<td><input id="intervalDays" name="intervalDays" value="">
			</td>
		</tr>

        <c:forEach var="i" items="${teams}">
            <tr>
                <td>
                    (${i})球队名字:
                </td>
                <td>
                    <input name="team_${i}" value="">
                </td>
            </tr>
        </c:forEach>

		<tr>
			<td></td>
			<td>
			<button class="btn btn-primary" type="submit" onclick="defineCourtAndTime()">定义场地和时间模板</button>
			</td>
		</tr>
	</table>
		

</div>
</form>

<script src="${contextPath}/js/jquery-1.8.3.min.js"></script>
<script src="${contextPath}/js/bootstrap.min.js"></script>
<script src="${contextPath}/js/bootstrap-datetimepicker.min.js"></script>
<script src="${contextPath}/js/locales/bootstrap-datetimepicker.fr.js"></script>

<script type="text/javascript">
$('.form_datetime').datetimepicker({
    language:  'fr',
    weekStart: 1,
    todayBtn:  1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 2,
	forceParse: 0,
    showMeridian: 1
});
$('.form_date').datetimepicker({
    //language:  'fr',
    weekStart: 1,
    todayBtn:  1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 2,
	minView: 2,
	forceParse: 0
});
    $('.form_time').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0
    });
</script>
