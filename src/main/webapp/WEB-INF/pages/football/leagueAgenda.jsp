<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include.jsp"%>
<script type="text/javascript">
	function save(){
		var url= "/foreversports/football/league/activeLeagueHomePage.html";
		$("#leagueAgendaForm").attr("action", url);
		$("#leagueAgendaForm").sumit();
	}
	
	function repeat(){
		var url="/foreversports/football/league/createAgenda.html";
		$("#createLeageuForm").attr("action", url);
		$("#createLeageuForm").sumit();
	}
</script>
    <form method="post" id="leagueAgendaForm" >
    <input name="leagueName" value="${leagueName}">
	<input name="teamNumber" value="${teamNumber}">
	<input name="matchType" value="${matchType}">
    <h1>
		Forever Sports
    </h1>
    <div class="hero-unit">
        <c:out value="${leagueName} 赛程安排："></c:out>
        <p>
            
            <c:forEach var="s" items="${list}" varStatus="status" >
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<c:out value="第 ${s.roundNum} 轮："/>
                <c:forEach var="pair" items="${s.againstPairs}" varStatus="status">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<c:out value="${pair}"/>,
                </c:forEach>
           
                <br/><br/>
            </c:forEach>
        </p>
        <button class="btn btn-primary" type="submit" onclick="repeat()">重新排赛程</button>
        <button class="btn btn-primary" type="submit" onclick="save()">保存</button>
    </div>
    </form>
