<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include.jsp"%>

<h1>Forever Sports</h1>
<div class="hero-unit">
	<table width="700px">
		<th>比赛日期</th>
		<th>比赛时间</th>
		<th></th>
		<th>对阵</th>
		<th></th>
		<th>比分</th>
		<c:forEach var="agenda" items="${list}" varStatus="status">
			<tr>
				<td align="middle"><fmt:formatDate value="${agenda.reacDate}" type="date"
						dateStyle="full" />
				</td>
				<td align="middle"><fmt:formatDate value="${agenda.firstHalfStartTime}" pattern="HH:mm" />
				</td>
				<td align="middle">
					<c:out value="${agenda.teamAName}" />
				</td>
				<td align="middle">VS</td>
				<td align="middle"> 
					<c:out value="${agenda.teamBName}" />
				</td>
				<td align="middle">
					<c:out value="${agenda.teamAScore}" />
						：
					<c:out value="${agenda.teamBScore}" />
				</td>
			</tr>
		</c:forEach>

	</table>

</div>
