<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include.jsp"%>

<script type="text/javascript">
	function createLeague(){
		var url="/foreversports/football/league/createAgenda.html";
		$("#createLeageuForm").attr("action", url);
		$("#createLeageuForm").sumit();
	}
</script>

<form method="post" id="createLeageuForm" action="createAgenda.html">
<h1>Forever Sports</h1>
<div class="hero-unit">
	<table width="700px">
		<tr>
			<td>联赛名称：</td>
			<td><input id="leagueName" name="leagueName">
			</td>
		</tr>
		<tr>
			<td>队伍数量：</td>
			<td><input id="teamNumber" name="teamNumber">
			</td>
		</tr>
		<tr>
			<td>联赛类型：</td>
			<td>
				<select id = "matchType" name="matchType">
				<option value="Double">双循环</option>
				<option value="Single">单循环</option>
				</select>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
			<button class="btn btn-primary" type="submit" onclick="createLeague()">提交</button>
			</td>
		</tr>
	</table>
		

</div>
</form>

<script src="${contextPath}/js/jquery-1.8.3.min.js"></script>
<script src="${contextPath}/js/bootstrap.min.js"></script>
<script src="${contextPath}/js/bootstrap-datetimepicker.min.js"></script>
<script src="${contextPath}/js/locales/bootstrap-datetimepicker.fr.js"></script>

<script type="text/javascript">
$('.form_datetime').datetimepicker({
    language:  'fr',
    weekStart: 1,
    todayBtn:  1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 2,
	forceParse: 0,
    showMeridian: 1
});
$('.form_date').datetimepicker({
    //language:  'fr',
    weekStart: 1,
    todayBtn:  1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 2,
	minView: 2,
	forceParse: 0
});
    $('.form_time').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0
    });
</script>
