<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="/common/include.jsp" %>

<script type="text/javascript">
    function finish() {
        var url = "/foreversports/football/league/finalAgenda.html";
        $("#createLeageuForm").attr("action", url);
        $("#createLeageuForm").sumit();
    }
</script>

<form method="post" id="createLeageuForm" action="createAgenda.html">
    <input name="matchNumber" value="${matchNumber}">
    <input name="leagueId" value="${leagueId}">
    <input name="fieldNumber" value="${fieldNumber}">
    <input name="leagueStartDate" value="${leagueStartDate}">
    <input name="intervalDays" value="${intervalDays}">

    <h1>Forever Sports</h1>

    <div class="hero-unit">

        <c:forEach var="i" begin="1" end="${fieldNumber}" step="1">

            <table>
                <tr>
                    <td>场地${i}名字:</td>
                    <td><input name="fieldName_${i}" value=""></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <c:forEach var="j" begin="1" end="${matchNumber}" step="1">
                                <tr>
                                    <td>上半场: </td>
                                    <td>
                                        <div class="controls input-append date form_time" data-date=""
                                             data-date-format="hh:ii" data-link-field="dtp_input1"
                                             data-link-format="hh:ii">
                                            <input width="20px" size="16" type="text" value="" readonly
                                                   name="firstHalfStartTime_${i}_${j}">
                                            <span class="add-on"><i class="icon-remove"></i></span>
                                            <span class="add-on"><i class="icon-th"></i></span>
                                        </div>
                                        <input type="hidden" id="dtp_input1" value=""/>
                                    </td>
                                    <td>
                                        <div class="controls input-append date form_time" data-date=""
                                             data-date-format="hh:ii" data-link-field="dtp_input2"
                                             data-link-format="hh:ii">
                                            <input width="20px" size="16" type="text" value="" readonly
                                                   name="firstHalfEndTime_${i}_${j}">
                                            <span class="add-on"><i class="icon-remove"></i></span>
                                            <span class="add-on"><i class="icon-th"></i></span>
                                        </div>
                                        <input type="hidden" id="dtp_input2" value=""/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>下半场: </td>
                                    <td>
                                        <div class="controls input-append date form_time" data-date=""
                                             data-date-format="hh:ii" data-link-field="dtp_input3"
                                             data-link-format="hh:ii">
                                            <input width="20px" size="16" type="text" value="" readonly
                                                   name="secondHalfStartTime_${i}_${j}">
                                            <span class="add-on"><i class="icon-remove"></i></span>
                                            <span class="add-on"><i class="icon-th"></i></span>
                                        </div>
                                        <input type="hidden" id="dtp_input3" value=""/>
                                    </td>
                                    <td>
                                        <div class="controls input-append date form_time" data-date=""
                                             data-date-format="hh:ii" data-link-field="dtp_input4"
                                             data-link-format="hh:ii">
                                            <input width="20px" size="16" type="text" value="" readonly
                                                   name="sencondHalfEndTime_${i}_${j}">
                                            <span class="add-on"><i class="icon-remove"></i></span>
                                            <span class="add-on"><i class="icon-th"></i></span>
                                        </div>
                                        <input type="hidden" id="dtp_input4" value=""/>
                                    </td>
                                </tr>
                                <tr style="height: 8px" />
                            </c:forEach>
                        </table>
                    </td>
                </tr>
            </table>

            <div style="height: 15px; width: 100%" />

        </c:forEach>

        <button class="btn btn-primary" type="submit" onclick="finish()">完成</button>

    </div>
</form>

<div style="height: 100px; width: 100%" />

<script src="${contextPath}/js/jquery-1.8.3.min.js"></script>
<script src="${contextPath}/js/bootstrap.min.js"></script>
<script src="${contextPath}/js/bootstrap-datetimepicker.min.js"></script>
<script src="${contextPath}/js/locales/bootstrap-datetimepicker.fr.js"></script>

<script type="text/javascript">
    $('.form_time').datetimepicker({
        language: 'fr',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0
    });

</script>
