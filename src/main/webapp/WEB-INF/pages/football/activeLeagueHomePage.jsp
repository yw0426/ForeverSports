<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include.jsp"%>

    <h1>
		Forever Sports
    </h1>
    <div class="hero-unit">
        <h1>
            Welcome!
        </h1>
        <p>
        	<c:forEach var="s" items="${leagueList}" varStatus="status" >
        		<a href="${contextPath}/football/league/defineLeagueDetail.html?leagueId=${s.leagueId}"><c:out value="${s.leagueName}"/></a>
        	</c:forEach>
        </p>

    </div>
