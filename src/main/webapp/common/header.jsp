<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include.jsp"%>

<div class="navbar-inner">
    <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a> <a class="brand" href="${contextPath}">Forever Sports</a>
        <div class="nav-collapse">
            <ul class="nav">
                <li class="active">
                    <a href="${contextPath}/football/index.html"> 足 球 </a>
                </li>
                <li>
                    <a href="${contextPath}/football/index.html"> 篮 球 </a>
                </li>
                <li>
                    <a href="${contextPath}/football/index.html"> 羽毛球 </a>
                </li>
            </ul>
            <ul class="nav pull-right">
                <li>
                    <a href="profile.htm">@username</a>
                </li>
                <li>
                    <a href="login.htm">Logout</a>
                </li>
            </ul>
        </div>
    </div>
</div>