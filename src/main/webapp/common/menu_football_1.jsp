<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/include.jsp"%>

<div class="well" style="padding: 8px 0;">
    <ul class="nav nav-list">
        <li class="nav-header">
            Forever Sports
        </li>
        <li class="active">
            <a href="/football/league/activeLeagueHomePage.html"><i class="icon-white icon-home"></i>正在进行中的联赛</a>
        </li>
        <li>
            <a href="index.html"><i class="icon-folder-open"></i>已经结束的联赛</a>
        </li>
        <li>
            <a href="league/viewCreateLeague.html"><i class="icon-check"></i>创建联赛</a>
        </li>
        <li>
            <a href="league/team.html"><i class="icon-check"></i>球队管理</a>
        </li>
    </ul>
</div>