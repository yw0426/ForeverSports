<%--
  Created by IntelliJ IDEA.
  User: zhoujl
  Date: 11/01/15
  Time: 10:08
  To change this template use File | Settings | File Templates.
--%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>

  <title>Signin</title>

  <!-- Bootstrap core CSS -->
  <link href="<c:url value="/static/css/bootstrap.min.css"/>" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<c:url value="/static/css/signin.css"/>" rel="stylesheet">
</head>

<body>

<c:forEach var="s" items="${list}" varStatus="status">
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<c:out value="第 ${s.num} 轮："/> 
	<br/>
	<c:forEach var="i" items="${s.agendaList}" varStatus="status">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<c:out value="${i}"/> , 
	</c:forEach> 
	<br/><br/>
</c:forEach>

</body>
</html>