DROP DATABASE IF EXISTS foreversports;
CREATE DATABASE foreversports DEFAULT CHARACTER SET utf8;
USE foreversports;

-- 场地表
DROP TABLE IF EXISTS T_COURT;
create table T_COURT(
	court_id int NOT NULL AUTO_INCREMENT,
	court_name varchar(200) not null,
	court_addrmess varchar(500) ,
	night_ind TINYINT(1),
	court_contact_phone varchar(15),
	court_contact_name varchar(50),
	PRIMARY KEY (court_id)
);

-- 球队表
DROP TABLE IF EXISTS T_TEAM;
CREATE TABLE T_TEAM(
	team_id int not null auto_increment,
	team_name varchar(200) not null,
	team_logo varchar(200),
	team_introduce varchar(500),
	primary key(team_id)
);


-- 队员表
drop table if exists T_PLAYER;
create table T_PLAYER(
	player_id int not null auto_increment,
	player_name varchar(200),
	player_phone varchar(15),
	player_id_no varchar(18),
	qq varchar(20),
	height int(3),
	weight int(3),
	shirt_size varchar(10),
	shoes_size varchar(10),
	primary key(player_id) 
);

-- 队员球队表
drop table if exists T_TEAM_MEMBER;
create table T_TEAM_MEMEBER(
	tm_id int not null auto_increment,
	player_id int not null,
	team_id int not null,
	primary key(tm_id)
);

-- 联赛表
drop table if exists T_LEAGUE;
CREATE TABLE T_LEAGUE(
	league_id int not null auto_increment,
	league_name varchar(200),
	sponsor varchar(200),
	founder varchar(200),
	phone varchar(15),
	qq varchar(20),
	league_introduce varchar(500),
	create_time datetime,
	start_time datetime,
	end_time datetime,
	league_type varchar(2),
	organizer varchar(200),
	primary key(league_id)
);

-- 赛程表
drop table if exists T_CALENDAR;
CREATE TABLE T_CALENDAR(
	cld_id int not null auto_increment,
	league_id int,
	round int(3),
	reac_date datetime,
	court_id int,
	first_half_start_time time,
	first_half_end_time time,
	second_half_start time,
	second_half_end time,
	team_1 int,
	team_1_score int(3),
	teamd_1_point int(3),
	team_2 int,
	team_2_score int(3),
	team_2_point int(3),
	primary key(cld_id)
);


-- 比赛明细表
drop table if exists T_MATCH_DETAIL;
CREATE TABLE T_MATCH_DETAIL(
	md_id int not null auto_increment,
	league_id int not null,
	cld_id int not null,
	team_id int not null,
	player_id int not null,
	occurrence_time time,
	detail_type varchar(3),
	primary key(md_id)
);

